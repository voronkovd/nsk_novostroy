# Сайт новостроек

Порядок установки:

```sh
$ git clone [git-repo-url]
$ virtaulenv env
$ source env/bin/activate
$ pip install -r requirements.txt
$ cd app/
$ python manage.py migrate
$ python manage.py runserver
```