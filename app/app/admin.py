# -*- coding: utf-8 -*-
from models import Area, Complex, Apartment, HotVariant, Home, GalleryComplex, GalleryHome, News
from django.contrib import admin
from django.forms import ModelForm
from django.contrib.admin import ModelAdmin
from suit_redactor.widgets import RedactorWidget
from django.utils.safestring import mark_safe

try:
    import json
except ImportError:
    import django.utils.simplejson as json


class MyRedactorWidget(RedactorWidget):

    class Media:
        def __init__(self):
            pass

        css = {
            'all': ('suit-redactor/redactor/redactor.css',)
        }
        js = ('suit-redactor/redactor/redactor.js',)

    def __init__(self, attrs=None, editor_options={}):
        super(RedactorWidget, self).__init__(attrs)
        self.editor_options = editor_options

    def render(self, name, value, attrs=None):
        output = super(RedactorWidget, self).render(name, value, attrs)
        output += mark_safe(
            '<script type="text/javascript">$("#id_%s").redactor(%s);</script>'
            % (name, json.dumps(self.editor_options)))
        return output


class ComplexForm(ModelForm):
    class Meta:
        widgets = {'description': MyRedactorWidget(editor_options={'lang': 'ru', 'minHeight': 300})}


class ApartmentForm(ModelForm):
    class Meta:
        widgets = {'description': MyRedactorWidget(editor_options={'lang': 'ru', 'minHeight': 300})}


class NewsForm(ModelForm):
    class Meta:
        widgets = {'content': MyRedactorWidget(editor_options={'lang': 'ru', 'minHeight': 300})}


class AreaAdmin(ModelAdmin):
    list_display = ('name',)
    list_filter = ('name',)
    list_per_page = 10
    list_max_show_all = 30
    save_as = True


class ComplexAdmin(ModelAdmin):
    exclude = ('admin_image',)
    list_display = ('area', 'name', 'admin_image')
    list_filter = ('area', 'name')
    list_select_related = True
    list_per_page = 10
    list_max_show_all = 30
    save_as = True


class HomeAdmin(ModelAdmin):
    list_display = ('complex', 'street', 'material', 'count_floors', 'end_of_period', 'period_status')
    list_filter = ('complex', 'street', 'material', 'count_floors', 'end_of_period', 'period_status')
    form = ComplexForm
    list_select_related = True
    list_per_page = 10
    list_max_show_all = 30
    save_as = True


class ApartmentAdmin(ModelAdmin):
    list_display = ('id', 'home', 'type', 'count_room', 'floor', 'price', 'place_all', 'place_live', 'place_kitchen', 'status')
    list_filter = ('id', 'home', 'type', 'count_room', 'floor', 'price', 'place_all', 'place_live', 'place_kitchen')
    list_select_related = True
    list_per_page = 10
    list_max_show_all = 30
    save_as = True
    form = ApartmentForm


class GalleryComplexAdmin(ModelAdmin):
    list_display = ('complex', 'admin_image')
    list_filter = ('complex',)
    list_select_related = True
    list_per_page = 10
    list_max_show_all = 30
    save_as = True


class GalleryHomeAdmin(ModelAdmin):
    list_display = ('home', 'admin_image')
    list_filter = ('home',)
    list_select_related = True
    list_per_page = 10
    list_max_show_all = 30
    save_as = True


class HotVariantAdmin(ModelAdmin):
    list_display = ('apartment',)
    list_filter = ('apartment',)
    list_select_related = True
    list_per_page = 10
    list_max_show_all = 30
    save_as = True


class NewsAdmin(ModelAdmin):
    list_display = ('title', 'admin_image', 'content', 'status')
    list_filter = ('title', 'content', 'status')
    search_fields = ('title', 'content')
    list_select_related = True
    list_per_page = 10
    list_max_show_all = 30
    save_as = True
    form = NewsForm


admin.site.register(Complex, ComplexAdmin)
admin.site.register(Home, HomeAdmin)
admin.site.register(Area, AreaAdmin)
admin.site.register(Apartment, ApartmentAdmin)
admin.site.register(GalleryComplex, GalleryComplexAdmin)
admin.site.register(GalleryHome, GalleryHomeAdmin)
admin.site.register(HotVariant, HotVariantAdmin)
admin.site.register(News, NewsAdmin)