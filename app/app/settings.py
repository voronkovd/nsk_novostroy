# -*- coding: utf-8 -*-
import os
import ConfigParser
from django.conf.global_settings import TEMPLATE_CONTEXT_PROCESSORS as TCP

BASE_DIR = os.path.dirname(os.path.dirname(__file__))
custom_config = ConfigParser.ConfigParser()
custom_config.readfp(open(BASE_DIR + '/app/production.conf'))
SECRET_KEY = custom_config.get('main', 'secret_key')
DEBUG = custom_config.getboolean('main', 'debug')
TEMPLATE_DEBUG = custom_config.getboolean('main', 'template_debug')
ADMINS = ((custom_config.get('main', 'admin_name'), custom_config.get('main', 'admin_email')),)
ALLOWED_HOSTS = ['*']
DEFAULT_APP = custom_config.get('main', 'default_app')
INSTALLED_APPS = (
    'yandex_maps',
    'suitlocale',
    'suit',
    'suit_redactor',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'app',
)
MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)
EMAIL_BACKEND = 'email_log.backends.EmailBackend'
TEMPLATE_CONTEXT_PROCESSORS = TCP + ('django.core.context_processors.request',)
ROOT_URLCONF = 'app.urls'
WSGI_APPLICATION = 'app.wsgi.application'
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': custom_config.get('mysql', 'name'),
        'USER': custom_config.get('mysql', 'user'),
        'PASSWORD': custom_config.get('mysql', 'password'),
        'HOST': '127.0.0.1',
    }
}
TEMPLATE_DIRS = (
    BASE_DIR + '/app/templates/',
)
LANGUAGE_CODE = custom_config.get('main', 'language')
TIME_ZONE = custom_config.get('main', 'timezone')
USE_I18N = True
USE_L10N = True
USE_TZ = True
STATIC_ROOT = BASE_DIR + '/static/'
MEDIA_URL = '/media/'
MEDIA_ROOT = BASE_DIR + '/media/'
STATICFILES_DIRS = (
    # os.path.join(BASE_DIR, 'static'),
)
STATIC_URL = '/static/'
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

SUIT_CONFIG = {
    'ADMIN_NAME': 'Админка',
    'SEARCH_URL': '',
    'HEADER_DATE_FORMAT': False,
    'HEADER_TIME_FORMAT': False,
    'CONFIRM_UNSAVED_CHANGES': False,
    'MENU_OPEN_FIRST_CHILD': True,
    'MENU': (
        {'label': u'Районы', 'icon': 'icon-folder-close', 'url': '/admin/app/area/'},
        {'label': u'Комплексы', 'icon': 'icon-folder-close', 'url': '/admin/app/complex/'},
        {'label': u'Дома', 'icon': 'icon-folder-close', 'url': '/admin/app/home/'},
        {'label': u'Квартиры', 'icon': 'icon-folder-close', 'url': '/admin/app/apartment/'},
        {'label': u'Галерея Комплексов', 'icon': 'icon-folder-close', 'url': '/admin/app/gallerycomplex/'},
        {'label': u'Галерея Домов', 'icon': 'icon-folder-close', 'url': '/admin/app/galleryhome/'},
        {'label': u'Горячие предложения', 'icon': 'icon-folder-close', 'url': '/admin/app/hotvariant/'},
        {'label': u'Новости', 'icon': 'icon-folder-close', 'url': '/admin/app/news/'},
    ),
}
ERROR_LOG_FILE = BASE_DIR + '/logs/' + 'error.log'
INFO_LOG_FILE = BASE_DIR + '/logs/' + 'info.log'
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'null': {
            'level': 'DEBUG',
            'class': 'django.utils.log.NullHandler',
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
        'production_error': {
            'level': 'ERROR',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': ERROR_LOG_FILE,
            'maxBytes': 1024 * 1024 * 5,
            'backupCount': 5,
            'formatter': 'verbose',
        },
        'production_info': {
            'level': 'INFO',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': INFO_LOG_FILE,
            'maxBytes': 1024 * 1024 * 5,
            'backupCount': 5,
            'formatter': 'verbose',
        },

    },
    'loggers': {
        'django': {
            'handlers': ['null'],
            'propagate': True,
            'level': 'INFO',
        },
        'django.request': {
            'handlers': ['production_error', 'production_info'],
            'level': 'ERROR',
            'propagate': False,
        },
    }
}

EMAIL_HOST = custom_config.get('email', 'smtp')
EMAIL_HOST_PASSWORD = custom_config.get('email', 'password')
EMAIL_HOST_USER = custom_config.get('email', 'user')
EMAIL_PORT = 465
EMAIL_USE_SSL = True
EMAIL_SUBJECT_PREFIX = custom_config.get('email', 'prefix')
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
        'LOCATION': '/tmp/',
    }
}
YANDEX_MAPS_API_KEY = 'AJ8Gr1QBAAAAighJOwIA-JE7U-RTwe0C4xCnzXs8QTTH4kAAAAAAAAAAAACACkm9NMNIwSvhcF5KORV-5Ja_GA=='