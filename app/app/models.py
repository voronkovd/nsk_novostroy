# -*- coding: utf-8 -*
from uuid import uuid4
from django.conf import settings
from django.db import models
import os


def path_and_rename(path):
    def wrapper(instance, filename):
        ext = filename.split('.')[-1]
        if instance.pk:
            filename = '{}.{}'.format(instance.pk, ext)
        else:
            filename = '{}.{}'.format(uuid4().hex, ext)
        return os.path.join(path, filename)

    return wrapper


class Area(models.Model):
    name = models.CharField(max_length=32, verbose_name=u"Наименование", help_text=u"")

    class Meta:
        db_table = 'areas'
        app_label = settings.DEFAULT_APP
        verbose_name = u'Район'
        verbose_name_plural = u'Районы'

    def __unicode__(self):
        return self.name


class Complex(models.Model):
    area = models.ForeignKey(Area, verbose_name=u"Район")
    name = models.CharField(max_length=32, verbose_name=u"Наименование")
    image = models.ImageField(verbose_name=u"Изображение", upload_to=path_and_rename('images/complex/'))

    class Meta:
        db_table = 'complexes'
        app_label = settings.DEFAULT_APP
        verbose_name = u'Комплекс'
        verbose_name_plural = u'Комплексы'

    def admin_image(self):
        return '<img style="max-width:100px;" src="/media/%s"/>' % self.image

    admin_image.allow_tags = True
    admin_image.short_description = U'Изображение'
    admin_image.admin_order_field = 'image'

    def __unicode__(self):
        return self.name


class Home(models.Model):
    WALL_MATERIALS = (
        (1, u'Бетон'),
        (2, u'Бетонно-монолитный каркас'),
        (3, u'Блоки'),
        (4, u'Газобетон'),
        (5, u'Дерево'),
        (6, u'Дерево обложенное кирпичом'),
        (7, u'Другое'),
        (8, u'Каркасно-насыпной'),
        (9, u'Кирпич'),
        (10, u'Кирпич на монолитном каркасе'),
        (11, u'Монолит'),
        (12, u'Панель'),
        (13, u'Панель-кирпич'),
        (14, u'Пенобетон'),
        (15, u'Сибит'),
        (16, u'Шлакоблоки'),
    )
    complex = models.ForeignKey(Complex, verbose_name=u"Комплекс")
    street = models.CharField(max_length=32, verbose_name=u"Адрес")
    material = models.IntegerField(choices=WALL_MATERIALS, verbose_name=u"Материал")
    description = models.CharField(max_length=1000, verbose_name=u"Описание")
    count_floors = models.IntegerField(max_length=2, verbose_name=u"Этажность")
    end_of_period = models.CharField(max_length=32, verbose_name=u"Дата сдачи")
    period_status = models.BooleanField(default=False, verbose_name=u"Дом сдан")

    class Meta:
        db_table = 'homes'
        app_label = settings.DEFAULT_APP
        verbose_name = u'Дом'
        verbose_name_plural = u'Дома'

    def __unicode__(self):
        return self.street


class Apartment(models.Model):
    TYPE_FLATS = (
        (1, u'Двухуровневая'),
        (2, u'Комната'),
        (3, u'Студия'),
        (4, u'Типовая'),
        (5, u'Улучшенной планировки'),
    )
    home = models.ForeignKey(Home, verbose_name=u"Дом")
    type = models.IntegerField(choices=TYPE_FLATS, verbose_name=u"Тип квартиры")
    count_room = models.IntegerField(max_length=1, verbose_name=u"Колличество комнат")
    floor = models.IntegerField(verbose_name=u"Этаж")
    price = models.IntegerField(verbose_name=u"Цена за м2")
    description = models.TextField(max_length=1000, verbose_name=u"Описание")
    place_all = models.DecimalField(max_digits=5, decimal_places=2, verbose_name=u"Общая площадь, м2")
    place_live = models.DecimalField(max_digits=5, decimal_places=2, verbose_name=u"Жилая площадь, м2")
    place_kitchen = models.DecimalField(max_digits=5, decimal_places=2, verbose_name=u"Площадь кухни, м2")
    status = models.BooleanField(default=True, verbose_name=u"Отображать на сайте")

    def __unicode__(self):
        return u'Квартира ID= ['+str(self.id)+']'

    class Meta:
        db_table = 'apartments'
        app_label = settings.DEFAULT_APP
        verbose_name = u'Квартиру'
        verbose_name_plural = u'Квартиры'


class GalleryComplex(models.Model):
    complex = models.ForeignKey(Complex, verbose_name=u"Комплекс")
    image = models.ImageField(upload_to=path_and_rename('images/gallery/complex/'), verbose_name=u"Изображение")

    class Meta:
        db_table = 'gallery_complex'
        app_label = settings.DEFAULT_APP
        verbose_name = u'Галерея Комплексов'
        verbose_name_plural = u'Галереи Комплексов'

    def __unicode__(self):
        return self.apartment.complex.name

    def admin_image(self):
        return '<img style="max-width:100px;" src="/media/%s"/>' % self.image

    admin_image.allow_tags = True
    admin_image.short_description = U'Изображение'
    admin_image.admin_order_field = 'image'


class GalleryHome(models.Model):
    home = models.ForeignKey(Home, verbose_name=u"Дом")
    image = models.ImageField(upload_to=path_and_rename('images/gallery/home/'), verbose_name=u"Изображение")

    class Meta:
        db_table = 'gallery_home'
        app_label = settings.DEFAULT_APP
        verbose_name = u'Галерея Домов'
        verbose_name_plural = u'Галереи Домов'

    def __unicode__(self):
        return self.apartment.home.complex.name

    def admin_image(self):
        return '<img style="max-width:100px;" src="/media/%s"/>' % self.image

    admin_image.allow_tags = True
    admin_image.short_description = U'Изображение'
    admin_image.admin_order_field = 'image'


class HotVariant(models.Model):
    apartment = models.ForeignKey(Apartment, verbose_name=u"Квартира")

    class Meta:
        db_table = 'hot_variants'
        app_label = settings.DEFAULT_APP
        verbose_name = u'Горячее предложение'
        verbose_name_plural = u'Горячие предложения'

    # def __unicode__(self):
    #     return str(self.apartment.id)



class News(models.Model):
    title = models.CharField(max_length=32, verbose_name=u"Заголовок")
    image = models.ImageField(upload_to=path_and_rename('images/news/'), verbose_name=u"Изображение")
    content = models.TextField(max_length=32, verbose_name=u"Новость")
    status = models.BooleanField(default=True, verbose_name=u"Отображать на сайте")

    class Meta:
        db_table = 'news'
        app_label = settings.DEFAULT_APP
        verbose_name = u'Новость'
        verbose_name_plural = u'Новости'

    def __unicode__(self):
        return self.title

    def admin_image(self):
        return '<img style="max-width:100px;" src="/media/%s"/>' % self.image

    admin_image.allow_tags = True
    admin_image.short_description = U'Изображение'
    admin_image.admin_order_field = 'image'