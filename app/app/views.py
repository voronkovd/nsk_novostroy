# -*- coding: utf-8 -*-
from django.template.response import TemplateResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from models import Area, News


def index(request):
    areas = Area.objects.order_by('name').all()

    return TemplateResponse(request, 'public/index.html', {'areas': areas})


def news(request):
    news_object = News.objects.all()
    paginator = Paginator(news_object, 25)
    page = request.GET.get('page')
    try:
        news_list = paginator.page(page)
    except PageNotAnInteger:
        news_list = paginator.page(1)
    except EmptyPage:
        news_list = paginator.page(paginator.num_pages)
    return TemplateResponse(request, 'public/news.html', {'news': news_list})