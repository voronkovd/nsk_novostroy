# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
                       url(r'^$', 'app.views.index'),
                       url(r'^news/$', 'app.views.news'),
                       url(r'^admin/', include(admin.site.urls)),
                       (r'^media/(?P<path>.*)$', 'django.views.static.serve', {
                           'document_root': settings.MEDIA_ROOT}),
                       (r'^static/(?P<path>.*)$', 'django.views.static.serve', {
                           'document_root': settings.STATIC_ROOT}),
)