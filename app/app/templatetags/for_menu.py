# -*- coding: utf-8 -*-
from django import template
from app.models import Area, Complex

register = template.Library()


@register.simple_tag()
def get_area():
    content = ''
    areas = Area.objects.order_by('name').all()
    if areas:
        for area in areas:
            content += '<li><a href="#">' + area.name + '</a></li>'
    return content


@register.simple_tag()
def get_complex():
    content = ''
    complexes = Complex.objects.order_by('-id').all()[:4]
    if complexes:
        for complex in complexes:
            content += '<li><a href="#">' + complex.name + '</a></li>'
    return content

