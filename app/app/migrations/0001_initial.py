# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import geoposition.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Apartment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('type', models.IntegerField(verbose_name='\u0422\u0438\u043f \u043a\u0432\u0430\u0440\u0442\u0438\u0440\u044b', choices=[(1, '\u0414\u0432\u0443\u0445\u0443\u0440\u043e\u0432\u043d\u0435\u0432\u0430\u044f'), (2, '\u041a\u043e\u043c\u043d\u0430\u0442\u0430'), (3, '\u0421\u0442\u0443\u0434\u0438\u044f'), (4, '\u0422\u0438\u043f\u043e\u0432\u0430\u044f'), (5, '\u0423\u043b\u0443\u0447\u0448\u0435\u043d\u043d\u043e\u0439 \u043f\u043b\u0430\u043d\u0438\u0440\u043e\u0432\u043a\u0438')])),
                ('count_room', models.IntegerField(max_length=1, verbose_name='\u041a\u043e\u043b\u043b\u0438\u0447\u0435\u0441\u0442\u0432\u043e \u043a\u043e\u043c\u043d\u0430\u0442')),
                ('floor', models.IntegerField(verbose_name='\u042d\u0442\u0430\u0436')),
                ('price', models.IntegerField(verbose_name='\u0426\u0435\u043d\u0430 \u0437\u0430 \u043c2')),
                ('description', models.TextField(max_length=1000, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435')),
                ('place_all', models.DecimalField(verbose_name='\u041e\u0431\u0449\u0430\u044f \u043f\u043b\u043e\u0449\u0430\u0434\u044c, \u043c2', max_digits=5, decimal_places=2)),
                ('place_live', models.DecimalField(verbose_name='\u0416\u0438\u043b\u0430\u044f \u043f\u043b\u043e\u0449\u0430\u0434\u044c, \u043c2', max_digits=5, decimal_places=2)),
                ('place_kitchen', models.DecimalField(verbose_name='\u041f\u043b\u043e\u0449\u0430\u0434\u044c \u043a\u0443\u0445\u043d\u0438, \u043c2', max_digits=5, decimal_places=2)),
                ('ad', geoposition.fields.GeopositionField(max_length=42)),
                ('status', models.BooleanField(default=True, verbose_name='\u041e\u0442\u043e\u0431\u0440\u0430\u0436\u0430\u0442\u044c \u043d\u0430 \u0441\u0430\u0439\u0442\u0435')),
            ],
            options={
                'db_table': 'apartments',
                'verbose_name': '\u041a\u0432\u0430\u0440\u0442\u0438\u0440\u0443',
                'verbose_name_plural': '\u041a\u0432\u0430\u0440\u0442\u0438\u0440\u044b',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Area',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=32, verbose_name='\u041d\u0430\u0438\u043c\u0435\u043d\u043e\u0432\u0430\u043d\u0438\u0435')),
            ],
            options={
                'db_table': 'areas',
                'verbose_name': '\u0420\u0430\u0439\u043e\u043d',
                'verbose_name_plural': '\u0420\u0430\u0439\u043e\u043d\u044b',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Complex',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=32, verbose_name='\u041d\u0430\u0438\u043c\u0435\u043d\u043e\u0432\u0430\u043d\u0438\u0435')),
                ('image', models.ImageField(upload_to=None, verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435')),
                ('area', models.ForeignKey(verbose_name='\u0420\u0430\u0439\u043e\u043d', to='app.Area')),
            ],
            options={
                'db_table': 'complexes',
                'verbose_name': '\u041a\u043e\u043c\u043f\u043b\u0435\u043a\u0441',
                'verbose_name_plural': '\u041a\u043e\u043c\u043f\u043b\u0435\u043a\u0441\u044b',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='GalleryComplex',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', models.ImageField(upload_to=None, verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435')),
                ('complex', models.ForeignKey(verbose_name='\u041a\u043e\u043c\u043f\u043b\u0435\u043a\u0441', to='app.Complex')),
            ],
            options={
                'db_table': 'gallery_complex',
                'verbose_name': '\u0413\u0430\u043b\u0435\u0440\u0435\u044f \u041a\u043e\u043c\u043f\u043b\u0435\u043a\u0441\u043e\u0432',
                'verbose_name_plural': '\u0413\u0430\u043b\u0435\u0440\u0435\u0438 \u041a\u043e\u043c\u043f\u043b\u0435\u043a\u0441\u043e\u0432',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='GalleryHome',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', models.ImageField(upload_to=None, verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435')),
            ],
            options={
                'db_table': 'gallery_home',
                'verbose_name': '\u0413\u0430\u043b\u0435\u0440\u0435\u044f \u0414\u043e\u043c\u043e\u0432',
                'verbose_name_plural': '\u0413\u0430\u043b\u0435\u0440\u0435\u0438 \u0414\u043e\u043c\u043e\u0432',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Home',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('street', models.CharField(max_length=32, verbose_name='\u0410\u0434\u0440\u0435\u0441')),
                ('material', models.IntegerField(verbose_name='\u041c\u0430\u0442\u0435\u0440\u0438\u0430\u043b', choices=[(1, '\u0411\u0435\u0442\u043e\u043d'), (2, '\u0411\u0435\u0442\u043e\u043d\u043d\u043e-\u043c\u043e\u043d\u043e\u043b\u0438\u0442\u043d\u044b\u0439 \u043a\u0430\u0440\u043a\u0430\u0441'), (3, '\u0411\u043b\u043e\u043a\u0438'), (4, '\u0413\u0430\u0437\u043e\u0431\u0435\u0442\u043e\u043d'), (5, '\u0414\u0435\u0440\u0435\u0432\u043e'), (6, '\u0414\u0435\u0440\u0435\u0432\u043e \u043e\u0431\u043b\u043e\u0436\u0435\u043d\u043d\u043e\u0435 \u043a\u0438\u0440\u043f\u0438\u0447\u043e\u043c'), (7, '\u0414\u0440\u0443\u0433\u043e\u0435'), (8, '\u041a\u0430\u0440\u043a\u0430\u0441\u043d\u043e-\u043d\u0430\u0441\u044b\u043f\u043d\u043e\u0439'), (9, '\u041a\u0438\u0440\u043f\u0438\u0447'), (10, '\u041a\u0438\u0440\u043f\u0438\u0447 \u043d\u0430 \u043c\u043e\u043d\u043e\u043b\u0438\u0442\u043d\u043e\u043c \u043a\u0430\u0440\u043a\u0430\u0441\u0435'), (11, '\u041c\u043e\u043d\u043e\u043b\u0438\u0442'), (12, '\u041f\u0430\u043d\u0435\u043b\u044c'), (13, '\u041f\u0430\u043d\u0435\u043b\u044c-\u043a\u0438\u0440\u043f\u0438\u0447'), (14, '\u041f\u0435\u043d\u043e\u0431\u0435\u0442\u043e\u043d'), (15, '\u0421\u0438\u0431\u0438\u0442'), (16, '\u0428\u043b\u0430\u043a\u043e\u0431\u043b\u043e\u043a\u0438')])),
                ('description', models.CharField(max_length=1000, verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435')),
                ('count_floors', models.IntegerField(max_length=2, verbose_name='\u042d\u0442\u0430\u0436\u043d\u043e\u0441\u0442\u044c')),
                ('end_of_period', models.CharField(max_length=32, verbose_name='\u0414\u0430\u0442\u0430 \u0441\u0434\u0430\u0447\u0438')),
                ('period_status', models.BooleanField(default=False, verbose_name='\u0414\u043e\u043c \u0441\u0434\u0430\u043d')),
                ('complex', models.ForeignKey(verbose_name='\u041a\u043e\u043c\u043f\u043b\u0435\u043a\u0441', to='app.Complex')),
            ],
            options={
                'db_table': 'homes',
                'verbose_name': '\u0414\u043e\u043c',
                'verbose_name_plural': '\u0414\u043e\u043c\u0430',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='HotVariant',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('apartment', models.ForeignKey(verbose_name='\u041a\u0432\u0430\u0440\u0442\u0438\u0440\u0430', to='app.Apartment')),
            ],
            options={
                'db_table': 'hot_variants',
                'verbose_name': '\u0413\u043e\u0440\u044f\u0447\u0435\u0435 \u043f\u0440\u0435\u0434\u043b\u043e\u0436\u0435\u043d\u0438\u0435',
                'verbose_name_plural': '\u0413\u043e\u0440\u044f\u0447\u0438\u0435 \u043f\u0440\u0435\u0434\u043b\u043e\u0436\u0435\u043d\u0438\u044f',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='News',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=32, verbose_name='\u0417\u0430\u0433\u043e\u043b\u043e\u0432\u043e\u043a')),
                ('image', models.ImageField(upload_to=None, verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435')),
                ('content', models.TextField(max_length=32, verbose_name='\u041d\u043e\u0432\u043e\u0441\u0442\u044c')),
                ('status', models.BooleanField(default=True, verbose_name='\u041e\u0442\u043e\u0431\u0440\u0430\u0436\u0430\u0442\u044c \u043d\u0430 \u0441\u0430\u0439\u0442\u0435')),
            ],
            options={
                'db_table': 'news',
                'verbose_name': '\u041d\u043e\u0432\u043e\u0441\u0442\u044c',
                'verbose_name_plural': '\u041d\u043e\u0432\u043e\u0441\u0442\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='galleryhome',
            name='home',
            field=models.ForeignKey(verbose_name='\u0414\u043e\u043c', to='app.Home'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='apartment',
            name='home',
            field=models.ForeignKey(verbose_name='\u0414\u043e\u043c', to='app.Home'),
            preserve_default=True,
        ),
    ]
